import pandas_datareader as pdr
from pandas import DataFrame
import datetime
import time
import math


class MeanMoveLine:
    moveLine = {}
    # 종목 / 코스닥,코스피( KS or KQ ) / 몇년치 / 몇개월치 / 몇일치
    def __init__(self, jongmok=None, type=None, howManyYear=None, howManyMonth=None, howMany=None, data=None,sqlData=True):
        self.moveLine.clear()
        #if data != None:
        if sqlData==True:
            self.data = data
            self.data['5']=None
            self.data['10']=None
            self.data['20']=None
            '''
            인덱스 = self.data[21:].index
            self.data.drop(인덱스,inplace=True)
            
            print(self.data)
            '''
            self.data.sort_index(ascending=True, inplace=True)
            # 종가 데이터
            self.list = []
            real = self.getRealData()
            self.list.append([real, '^r--'])

            # 500초 평균선
            first = self.makeMeanData(5)
            self.list.append([first, 'g'])

            # 5000초 평균선
            sec = self.makeMeanData(10)
            self.list.append([sec, 'b'])

            # 20일 평균선

            th = self.makeMeanData(20)
            self.list.append([th, 'g'])

            self.makeMoveLine(self.list)
            #self.getDeviation(self.list)
            #self.makeGrape(self.list)

        else:
            tm = datetime.datetime.now()
            startTm = str(tm.year - howManyYear) + "-" + str(tm.month - howManyMonth) + "-" + str(tm.day - howMany)
            endTm = str(tm.year) + "-" + str(tm.month) + "-" + str(tm.day)

            # startTm 부터 endTm까지의 주식 데이터 가져오기
            self.data = pdr.get_data_yahoo(jongmok + '.' + type, start=startTm, end=endTm)


            '''
            ## 현재가 추가를 위한 방법
            new_raw = DataFrame([(0,0,0,0,0,50000)],columns=self.data.columns,index=[datetime.datetime.now()])
            self.data = self.data.append(new_raw)
'''
            print(self.data)

            self.list = []

            # 종가 데이터
            real = self.getRealData()
            self.list.append([real, '^r--'])

            # 5일 평균선
            first = self.makeMeanData(5)
            self.list.append([first, 'g'])

            # 10일 평균선
            sec = self.makeMeanData(10)
            self.list.append([sec, 'b'])

            # 20일 평균선
            th = self.makeMeanData(20)
            self.list.append([th, 'y'])

            self.getDeviation(self.list)
            self.makeGrape(self.list)

    def makeMoveLine(self,list):

        for i in list:
            for j in i[0].keys():
                if self.moveLine.__contains__(j):
                    self.moveLine[j].append(i[0][j])
                else:
                    self.moveLine[j] = [i[0][j], ]

        for i in self.moveLine:
            self.data.loc[i,'5'] = self.moveLine[i][1]
            self.data.loc[i, '10'] = self.moveLine[i][2]
            self.data.loc[i, '20'] = self.moveLine[i][3]

    def returnMoveLine(self):
        return self.moveLine


    def currentBuyAndSell(self,pre,cur):

        #파는 조건(매도)
        if self.isRealRightUp(pre, cur) and self.is5DayMeanRightUp(pre,cur) and self.isUP5to10(cur):
            return 10
        #사는 조건 (매수)
        elif (self.isRightDown(pre, cur) and self.is5DayMeanRightDown(pre, cur) and self.isDown5to10(cur)) or (self.isDecrease(pre, cur)):
            return -10
        else:
            return 0
    def getDeviation(self,list1):
        listDict = {}

        for i in list1:
            for j in i[0].keys():
                if listDict.__contains__(j):
                    listDict[j].append(i[0][j])
                else:
                    listDict[j] = [i[0][j], ]
        for item in list1:
            eachList = []
            keyList = []
            for each in item[0]:
                eachList.append(each)
            for key in item[0].index:
                keyList.append(key)


        key = sorted(listDict)
        print(listDict)
        sellList = []
        sellList.append(None)
        buyList = []
        buyList.append(None)
        pre = key.pop(0)
        # 현재 샀으면 True 팔았으면 False
        st = True
        buyPrice = 0
        print("keyLen",len(key))
        for i in key:
            # 우상향일 경우
            if True:
                # 파는 조건
                #if self.isRealRightUp(listDict[pre], listDict[i]) and self.is5DayMeanRightUp(listDict[pre], listDict[ i]) and self.isUP5to10(listDict[i]):
                if  self.isRealRightUp(listDict[pre],listDict[i]) and self.is5DayMeanRightUp(listDict[pre],listDict[i]) and self.isUP5to10(listDict[i]) or (buyPrice - buyPrice*0.02 > int(listDict[pre][0])):
                    buyList.append(listDict[i][0])
                    sellList.append(None)
                    st = True
                    if (buyPrice - buyPrice*0.01 > int(listDict[pre][0])):
                        print("1% 이하")
                        print("산가격",buyPrice)
                        print("판가격",listDict[pre][0])

                # 사는 조건
                elif (self.isRightDown(listDict[pre], listDict[i]) and self.is5DayMeanRightDown(listDict[pre],listDict[i]) and self.isDown5to10(listDict[i])) or (self.isDecrease(listDict[pre], listDict[i])):
                    sellList.append(listDict[i][0])
                    buyList.append(None)
                    st = False
                    buyPrice = int(listDict[i][0])
                else:
                    sellList.append(None)
                    buyList.append(None)
            # 우 하향일 경우

            pre = i


        key = sorted(listDict)

        pre = key.pop(0)
        for num, i in enumerate(key):
            pre = i


        ## 매수 매도 확인
        isSell = False
        tot = 0
        cell = 0
        totCnt =0

        print(len(keyList))
        print(len(sellList))
        print(len(buyList))
        for i in range(len(sellList)):
            if isSell == False and sellList[i] == None:
                continue
            elif isSell == False and sellList[i] != None:
                print()
                print("------- 매 수 -------")
                print("매수일 : ", keyList[i])
                print("매수금 : ", sellList[i])
                cell = int(sellList[i])
                cellDay = keyList[i]
                isSell = True
            elif isSell == True and buyList[i] != None:
                print("------- 매 도 -------")
                print("매도일 : ", keyList[i])
                print("매도금 : ", buyList[i])
                print("투자일 : ", int(keyList[i]) - int(cellDay))
                tex = int(buyList[i]) * 0.0025
                susu = int(buyList[i]) * 0.00015

                cell = int(buyList[i]) - cell - susu
                print("금번 손익 : ", cell)
                tot = tot + cell
                totCnt = totCnt+1
                isSell = False
            else:
                continue
        print("TOT : ", tot)
        print("거래 횟수 :",totCnt)
        print(listDict)
        print(list(listDict.values())[-1])

    def getRealData(self):
        # 종가 데이터
        ret = self.data['현재가']
        ret.__setattr__("name", "real")
        return ret

    def makeMeanData(self, standard):
        # standard (5일,10일,20일 etc...)에 맞게 rolling 후 평균 산출
        mean = self.data['현재가'].rolling(window=standard).mean()
        mean.__setattr__("name", str(standard))
        return mean

    def returnAllData(self):
        listDict = {}

        for i in self.list:
            for j in i[0].keys():
                if listDict.__contains__(j):
                    listDict[j].append(i[0][j])
                else:
                    listDict[j] = [i[0][j], ]

        return listDict

    ## list = [data , 그래프 색상]
    def makeGrape(self, list):
        listDict = {}

        for i in list:
            for j in i[0].keys():
                if listDict.__contains__(j):
                    listDict[j].append(i[0][j])
                else:
                    listDict[j] = [i[0][j], ]

        for item in list:
            eachList = []
            keyList = []
            for each in item[0]:
                eachList.append(each)
            for key in item[0].index:
                keyList.append(key)


        key = sorted(listDict)
        print(listDict)
        sellList = []
        sellList.append(None)
        pre = key.pop(0)
        for i in key:
            ## 조건 : Real 우상향 and 5일선 우상향 and 5일선이 10일선보다 클시
            if self.isRealRightUp(listDict[pre], listDict[i]) and self.is5DayMeanRightUp(listDict[pre], listDict[i]) and self.isUP5to10(listDict[i]):
                ## 조건 : Real 우상향 and 5일선 우상향 and 5일선이 10일선보다 클 시 and 하루에 1000원 이상 올랐을 시
                # if (self.isRealRightUp(listDict[pre], listDict[i]) and self.is5DayMeanRightUp(listDict[pre], listDict[i]) and self.isUP5to10(listDict[i])) or self.isIncrease(listDict[pre], listDict[i]):
                sellList.append(listDict[i][0])
            else:
                sellList.append(None)
            pre = i

        key = sorted(listDict)
        buyList = []
        buyList.append(None)
        pre = key.pop(0)
        for i in key:
            ## 조건 : Real 우하향 and 5일선 우하향 and 5일선이 10일선보다 작을 시
            # if self.isRightDown(listDict[pre],listDict[i]) and self.is5DayMeanRightDown(listDict[pre],listDict[i]) and self.isDown5to10(listDict[i]):
            ## 조건 : Real 우하향 and 5일선 우하향 and 5일선이 10일선보다 작을 시 and 하루에 1000원 이상 떨어졌을 시
            if (self.isRightDown(listDict[pre], listDict[i]) and self.is5DayMeanRightDown(listDict[pre], listDict[i]) and self.isDown5to10(listDict[i])) or (self.isDecrease(listDict[pre], listDict[i])):
                buyList.append(listDict[i][0])
            else:
                buyList.append(None)
            pre = i

        ## 매수 매도 확인
        isSell = False
        tot = 0
        cell = 0
        for i in range(len(sellList)):
            if isSell == False and sellList[i] == None:
                continue
            elif isSell == False and sellList[i] != None:
                print()
                print("------- 매 수 -------")
                print("매수일 : ", keyList[i])
                print("매수금 : ", sellList[i])
                cell = sellList[i]
                cellDay = keyList[i]
                isSell = True
            elif isSell == True and buyList[i] != None:
                print("------- 매 도 -------")
                print("매도일 : ", keyList[i])
                print("매도금 : ", buyList[i])
                print("투자일 : ", keyList[i] - cellDay)
                cell = buyList[i] - cell
                print("금번 손익 : ", cell)
                tot = tot + cell
                isSell = False
            else:
                continue

        print("TOT : ", tot)


    ## 매수 조건
    def isRightUp(self, previous, current):
        if previous < current:
            return True
        else:
            return False

    def isRealRightUp(self, previous, current):
        return self.isRightUp(previous[0], current[0])

    def is5DayMeanRightUp(self, previous, current):
        return self.isRightUp(previous[1], current[1])

    def is10DayMeanRightUp(self, previous, current):
        return self.isRightUp(previous[2], current[2])

    def is20DayMeanRightUp(self, previous, current):
        return self.isRightUp(previous[3], current[3])

    def isUP5to10(self, current):
        return self.isRightUp(current[2], current[1])

    def isIncrease(self, previous, current):
        if self.isRealRightUp(previous, current):
            if current[0] - previous[0] > previous[0] * 0.1:
                return True
        return False

    def isUp10to20(self,current):
        return self.isRightUp(current[3], current[2])

    def isUpPercent(self, previous, current,per):
        return self.isRightUp(previous[1], current[1]+current[1]*0.01*per)

    ## 매도 조건
    def isRightDown(self, previous, current):
        if previous > current:
            return True
        else:
            return False

    def isRealRightDown(self, previous, current):
        return self.isRightDown(previous[0], current[0])

    def is5DayMeanRightDown(self, previous, current):
        return self.isRightDown(previous[1], current[1])

    def is10DayMeanRightDown(self, previous, current):
        return self.isRightDown(previous[2], current[2])

    def is20DayMeanRightDown(self, previous, current):
        return self.isRightDown(previous[3], current[3])

    def isDown5to10(self, current):
        return self.isRightDown(current[2], current[1])

    def isDecrease(self, previous, current):
        if self.isRealRightDown(previous, current):
            if int(previous[0]) - int(current[0]) > int(previous[0]) * 0.1:
                return True
        return False

    def isDown10to20(self, current):
        return self.isRightDown(current[3], current[2])

    def isDownPercent(self, previous, current,per):
        return self.isRightDown(previous[1], current[1]-current[1]*0.01*per)
