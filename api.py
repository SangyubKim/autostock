import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QAxContainer import *
import datetime
from threading import Thread
import time
import cx_Oracle as cx
import pandas as pd
from MeanMoveLine import MeanMoveLine
import traceback

class MyWindow(QMainWindow):
    infoDic = {}
    priCnt = '0'
    tot = 0
    jongmok = "091180"
    cost = []
    guCnt = []
    sqlList=[]
    timeDict={}
    def __init__(self):
        super().__init__()
        try:


            self.setWindowTitle("PyStset Cock")
            self.setGeometry(300, 300, 300, 500)

            self.kiwoom = QAxWidget("KHOPENAPI.KHOpenAPICtrl.1")
            btn1 = QPushButton("Login", self)
            btn1.move(20, 20)
            btn1.clicked.connect(self.btn1_clicked)

            btn2 = QPushButton("Check state", self)
            btn2.move(20, 70)
            btn2.clicked.connect(self.btn2_clicked)

            btn3 = QPushButton("get ID state", self)
            btn3.move(20, 120)
            btn3.clicked.connect(self.btn3_clicked)

            btn4 = QPushButton("order", self)
            btn4.move(20, 170)
            btn4.clicked.connect(self.btn4_clicked)

            btn5 = QPushButton("realData Save", self)
            btn5.move(20, 220)
            btn5.clicked.connect(self.btn5_clicked)

            btn6 = QPushButton("real", self)
            btn6.move(20, 270)
            btn6.clicked.connect(self.btn6_clicked)

            btn7 = QPushButton("auto", self)
            btn7.move(20, 320)
            btn7.clicked.connect(self.btn7_clicked)


            btn8 = QPushButton("실시간호가", self)
            btn8.move(20, 370)
            btn8.clicked.connect(self.btn8_clicked)

            btn9 = QPushButton("분봉데이터", self)
            btn9.move(20, 420)
            btn9.clicked.connect(self.btn9_clicked)

            btn10 = QPushButton("보유주식식", self)
            btn10.move(20, 450)
            btn10.clicked.connect(self.btn10_clicked)


            self.kiwoom.OnReceiveRealData.connect(self.test3Real)
            self.kiwoom.OnReceiveTrData.connect(self.test)
            self.kiwoom.OnReceiveChejanData.connect(self.test2)

            x = Thread(target=self.dbInsertThread, args=())
            x.daemon=True
            x.start()
        except Exception as e:
            print(e)


    def btn7_clicked(self):
        x = Thread(target=self.auto, args=())
        x.start()

    def auto(self):
        #sys.exit()
        while True:
            curTime = datetime.datetime.now()
            hour = curTime.hour
            min = curTime.minute
            print(curTime)
            if hour == 9 and min==0 :
                self.btn1_clicked()
                st = self.btn2_clicked()
                if st == True:
                    self.btn6_clicked()
            else:
                time.sleep(1)
                if hour == 15 and min == 0 :
                    pass





    def dbInsertThread(self):
        print("dbInsert Thread Start")
        cx.init_oracle_client('C:/Users/yunmi/Downloads/instantclient-basic-nt-19.6.0.0.0dbru/instantclient_19_6')
        dsn = cx.makedsn("localhost", 1521, service_name="master")
        self.conn = cx.connect("c##masteruser", "q7w5r1z8", dsn)
        while True:
            print(len(self.sqlList))
            cursor = self.conn.cursor()
            list = self.sqlList[:]
            self.sqlList.clear()
            for data in list:
                try:
                    sql = data
                    print(sql)
                    cursor.execute(sql)
                    self.conn.commit()
                except Exception as e:
                    print(e)
                    print(data)
                    self.sqlList.append(data)

            self.conn.commit()
            cursor.close()
            time.sleep(5)


    def test2(self, sGunbun, sNItemCnt, sFidList):
        print("--------- 주식 체결 -----------")
        print(sGunbun)
        print(sNItemCnt)
        print(sFidList)
        if sGunbun == '0':
            '''
            Real Type : 주식체결
            [20] = 체결시간
            [10] = 현재가
            [11] = 전일대비
            [12] = 등락율
            [27] = (최우선)매도호가
            [28] = (최우선)매수호가
            [15] = 거래량
            [13] = 누적거래량
            [14] = 누적거래대금
            [16] = 시가
            [17] = 고가
            [18] = 저가
            [25] = 전일대비기호
            [26] = 전일거래량대비(계약,주)
            [29] = 거래대금증감
            [30] = 전일거래량대비(비율)
            [31] = 거래회전율
            [32] = 거래비용
            [228] = 체결강도
            [311] = 시가총액(억)
            [290] = 장구분
            [691] = KO접근도
            [567] = 상한가발생시간
            [568] = 하한가발생시간
            '''
            ch = self.kiwoom.dynamicCall("GetChejanData(int)", [10])
            print(ch)
        elif sGunbun == '1':
            '''
            Real Type : 잔고
            [9201] = 계좌번호
            [9001] = 종목코드,업종코드
            [917] = 신용구분
            [916] = 대출일
            [302] = 종목명
            [10] = 현재가
            [930] = 보유수량
            [931] = 매입단가
            [932] = 총매입가
            [933] = 주문가능수량
            [945] = 당일순매수량
            [946] = 매도/매수구분
            [950] = 당일총매도손일
            [951] = 예수금
            [27] = (최우선)매도호가
            [28] = (최우선)매수호가
            [307] = 기준가
            [8019] = 손익율
            [957] = 신용금액
            [958] = 신용이자
            [918] = 만기일
            [990] = 당일실현손익(유가)
            [991] = 당일실현손익률(유가)
            [992] = 당일실현손익(신용)
            [993] = 당일실현손익률(신용)
            [959] = 담보대출수량
            [924] = Extra Item
            '''
            ch = self.kiwoom.dynamicCall("GetChejanData(int)", [930])
            print(ch)


    def th(self, sJongmokCode, sRealType, sRealData):
        tm = datetime.datetime.now()
        print(sRealType)
        if sRealType == "주식체결":
            sp = sRealData.split('\t')
            if self.timeDict.keys().__contains__(sp[0]):
                self.timeDict[sp[0]]=self.timeDict[sp[0]]+1
            else:
                self.timeDict[sp[0]]=1

            cnt = self.timeDict[sp[0]]
            # 체결시간 / 현재가 / 전일대비 / 등락율 / 매도호가 / 매수호가 / 거개량 / 누적거래량 / 누적거래대금 / 시가 / 고가 / 저가 / 전일대비기호 / 전일거래량대비(계약,주) / 거래대금증감 / 전일거래량대비(비율) / 거래회전율 / 거래비용 / 체결강도 / 시가 총액 / 장구분 / KO접근도 / 상한가발생시간 / 하한가발생시간
            #print("시간: " +str(tm.date())+" "+ sp[0]+ " / 현재가: " + sp[1] + " / 전일대비:" + sp[2] + " / 등락율:" + sp[3] + " / 거래량:" + sp[6] + " / 매도수 거래량:" + str(self.tot))
            sql = 'INSERT INTO STOCK_REALDATA2 Values(\''+str(tm.date())+" "+ sp[0]+'\','+str(cnt)+','+sp[1]+','+sp[2]+','+sp[3]+','+sp[4]+','+sp[5]+','+sp[6]+','+sp[7]+','+sp[8]+','+sp[9]+','+sp[10]+','+sp[12]+','+sp[13]+','+sp[14]+','+sp[15]+','+sp[16]+','+sp[17]+','+sp[18]+','+sp[19]+','+sp[20]+','+sp[21]+','+sp[12]+',sysdate,'+self.jongmok+')'
            self.sqlList.append(sql)
        elif sRealType =='주식호가잔량':
            # 시간 / 매도호가1 / 매도호가수량1 / 매도호가직전대비1 / 매수호가 / 매수호가수량1 / 매수호가직전대비1 /  ...2~10 / 매도호가총잔량 / 매도호가총잔량직전대비 / 매수호가총잔량 / 매수호가총잔량직전대비
            print(sRealData)
            sp = sRealData.split('\t')
            print(sp[0])


        '''
        f = open('C:/test/samsung.csv', 'a')
        f.write(sp[0]+','+sp[1]+','+sp[2]+','+sp[3]+','+sp[4]+','+sp[5]+','+sp[6] + '\n')
        f.close()
        '''

    def test3Real(self, sJongmokCode, sRealType, sRealData):
        self.x = Thread(target=self.th, args=(sJongmokCode, sRealType, sRealData))
        self.x.start()

    def btn1_clicked(self):
        ret = self.kiwoom.dynamicCall("CommConnect()")

    def btn2_clicked(self):
        print(self.kiwoom.dynamicCall("GetConnectState()"))
        if self.kiwoom.dynamicCall("GetConnectState()") == 0:
            self.statusBar().showMessage("Not connected")
            return False
        else:
            self.statusBar().showMessage("Connected")
            return True

    def btn3_clicked(self):
        ret = self.kiwoom.dynamicCall("GetLoginInfo(string)", "ACCNO")
        print(ret)

    def btn4_clicked(self):
        # 사용자 구분 요청명 , 화면번호 , 계좌번호, 주문유형, 주식종목코드, 주문수량, 주문단가, 거래 구분, 원주문번호
        ret = self.kiwoom.dynamicCall("SendOrder(QString,QString,QString,int,QString,int,int,QString,QString)",["RQ_1", "0101", "8138308711", 1, self.jongmok, 5, 0, "03", ""])
        print(ret)

        # self.kiwoom.OnReceiveTrData.connect(self._receive_tr_data)

    def btn5_clicked(self):
        # while True:
        ret = self.kiwoom.dynamicCall("SetInputValue(QString,QString)", "종목코드", self.jongmok)
        ret = self.kiwoom.dynamicCall("CommRqData(QString,QString,int,QString)", ["RQ_NAME", "OPT10001", 0, "0101"])
        # time.sleep(0.5)

    def btn6_clicked(self):
        ret = self.kiwoom.dynamicCall("SetRealReg(QString,QString,QString,QString)", "0101", self.jongmok, "10", "0")

    def btn8_clicked(self):
        print("실시간 호가")
        self.kiwoom.dynamicCall("SetInputValue(QString,QString)", "종목코드", self.jongmok)
        self.kiwoom.dynamicCall("CommRqData(QString,QString,int,QString)", ["RQ_NAME", "OPT10004", 0, "0101"])

    def btn9_clicked(self):
        print("분봉")
        self.kiwoom.dynamicCall("SetInputValue(QString,QString)", "종목코드", self.jongmok)
        self.kiwoom.dynamicCall("CommRqData(QString,QString,int,QString)", ["RQ_NAME", "OPT10080", 0, "0101"])

    def btn10_clicked(self):
        self.kiwoom.dynamicCall("SetInputValue(QString,QString)", "계좌번호", 8138308711)
        self.kiwoom.dynamicCall("SetInputValue(QString,QString)", "비밀번호", 2550)
        self.kiwoom.dynamicCall("SetInputValue(QString,QString)", "상장폐지조회구분", 0)
        self.kiwoom.dynamicCall("CommRqData(QString,QString,int,QString)", ["RQ_NAME", "OPW00004", 0, "0101"])


    def test(self, sScrNo, sRQName, sTRCode, sRecordName, sPreNext, nDataLength, sErrorCode, sMessage, sSPlmMsg,
             **kwargs):
        print("---test start---")
        print(sRQName)
        print(sTRCode)
        print(sRecordName)
        print(sPreNext)
        print(nDataLength)
        print(sErrorCode)
        print(sMessage)
        print(sSPlmMsg)
        print(kwargs)
        try:
            now = datetime.datetime.now()
            time = "{}-{}-{} {}:{}:{}".format(now.year, now.month, now.day, now.hour, now.minute, now.second)
            if sTRCode =='OPT10080':
                print("분봉 조회")
                # 현재가 / 거래량 / 체결시간 / 시가 / 고가 / 저가
                stick = self.kiwoom.dynamicCall("GetCommDataEx(QString,QString",[sTRCode,"주식분차트"])
                print(len(stick))
                df = pd.DataFrame(columns=['현재가','거래량','시가','고가','저가'])
                for data in stick:
                    df.loc[data[2]]={'현재가':data[0].replace('+','').replace('-',''),'거래량':data[1],'시가':data[3].replace('+','').replace('-',''),'고가':data[4].replace('+','').replace('-',''),'저가':data[5].replace('+','').replace('-','')}

                info = MeanMoveLine(data=df)

                print(info.data)
                print(info.data.iloc[-1])
                print(info.data.iloc[-2])
                info.getDeviation(info.list)
                info.currentBuyAndSell(list(info.moveLine)[-1],list(info.moveLine)[-2])

            elif sTRCode == "OPW00004":
                print("계좌")
                보유수량 = int(self.kiwoom.dynamicCall("GetCommData(QString,QString,int,QString)", [sTRCode, "계좌평가현황요청", 0, "보유수량"]))
                종목코드 = self.kiwoom.dynamicCall("GetCommData(QString,QString,int,QString)", [sTRCode, "계좌평가현황요청", 0, "종목코드"])
                print(종목코드)
                print(보유수량)
            else :
                test1 = self.kiwoom.dynamicCall("GetCommData(QString,QString,int,QString)", [sTRCode, "주식기본정보", 0, "외인보유"])
                test2 = self.kiwoom.dynamicCall("GetCommData(QString,QString,int,QString)", [sTRCode, "주식기본정보", 0, "외인비중"])
                print(self.priCnt)
                print(time)
                print(test1.strip())
                print(test2.strip())
                cnt = int(test2.strip()) - int(self.priCnt);
                self.priCnt = str(test2.strip())
                print(cnt)
                print("---test end---")
                if self.infoDic.keys().__contains__(time):
                    temp = self.infoDic[time][1] + cnt
                    self.infoDic[time][1] = temp
                    print("merge: " + str(self.infoDic[time][1]))
                else:
                    self.infoDic[time] = [test1.strip(), cnt]

                sql = "INSERT INTO STOCK_REALDATA Values('" + time + "'," + test1 + ',' + test2 + ',null,null,null,' + test2 + ',' + self.jongmok + ',sysdate)'
                #self.sqlList.append(sql)

        except Exception as e:
            print(e)
            print(traceback.format_exc())


    def _receive_tr_data(self, screen_no, rqname, trcode, record_name, next, unused1, unused2, unused3, unused4):
        print(screen_no)
        print(rqname)
        print(trcode)
        print(record_name)
        print(next)
        print(unused1)
        print(unused2)
        print(unused3)
        print(unused4)

    def res(self, gubun, item_cnt, fid_list):
        print(gubun)
        print(self.get_chejan_data(9203))
        print(self.get_chejan_data(302))
        print(self.get_chejan_data(900))
        print(self.get_chejan_data(901))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    myWindow = MyWindow()
    myWindow.show()
    app.exec_()